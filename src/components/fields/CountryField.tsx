import { Combobox } from "@headlessui/react";
import { Combobox as TCombobox } from "./useFields";
import { useQuery } from "@tanstack/react-query";
import findCountries, { Country } from "../../country-service";
import { useState } from "react";

type CountryFieldProps = TCombobox;

export function CountryField({ id }: CountryFieldProps) {
  const [selectedCountry, setSelectedCountry] = useState<Country | null>(null);
  const [userInput, setUserInput] = useState("");
  const { data } = useQuery({
    queryKey: ["countries"],
    queryFn: findCountries,
  });
  const filteredCountries =
    userInput === ""
      ? data
      : data?.filter((country) =>
          country.name.common.toLowerCase().includes(userInput.toLowerCase())
        );

  return (
    <Combobox value={selectedCountry} onChange={setSelectedCountry} name={id}>
      <Combobox.Input onChange={(event) => setUserInput(event.target.value)} />
      <Combobox.Options>
        {filteredCountries &&
          filteredCountries.map((country) => (
            <Combobox.Option key={country.code} value={country}>
              {country.name.common}
            </Combobox.Option>
          ))}
      </Combobox.Options>
    </Combobox>
  );
}
