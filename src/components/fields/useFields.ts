import { v4 } from "uuid"
import { create } from "zustand"

export type Submit = {
    type: "submit",
    text: string,
    id: string
    label?: never
    placeholder?: never
}
export type Input = {
    type: "text" | "date" | "number",
    text?: never,
    placeholder: string,
    id: string,
    label: string | null
}
export type Combobox = {
    type: "country",
    text?: never,
    id: string,
    label: string | null
    placeholder: string,
}
export type Field = Input | Submit | Combobox


const generateRandomUUID = () => {
    return v4() // this may be replaced with other uuid generators
}
const INIT_FIELDS: Field[] = [
    { type: "text", placeholder: "Name", label: null, id: generateRandomUUID() },
    { type: "number", placeholder: "Age", label: null, id: generateRandomUUID() },
    { type: "date", placeholder: "Date of Birth", label: null, id: generateRandomUUID() },
    { type: "country", placeholder: "Country", label: null, id: generateRandomUUID() },
    { type: "submit", text: "submit", id: generateRandomUUID() }
]

interface FieldState {
    fields: Field[]
    update: (field: Partial<Field>, id: string) => void;
    add: (field: Field) => void;
    remove: (field: Field) => void;
}

/**
 * A store for managing form fields
 **/
export const useFieldStore = create<FieldState>()(
    (set) => ({
        fields: [],
        update: (field, id) => set((state) => {
            const newFields = [...state.fields];
            const currentField = newFields.find(f => f.id === id)
            if (!currentField) {
                return {
                    fields: state.fields
                }
            }
            const index = newFields.indexOf(currentField);
            const result = { ...currentField, ...field }
            newFields[index] = result as Field;
            return {
                fields: newFields
            }
        }),
        add: (field) => set((state) => {
            return {fields: [...state.fields, field]}
        }),
        remove: (field) => set((state) => {
            return {fields: state.fields.filter(f => f.id !== field.id)}
        })
    })
)

/**
 * A hook for managing form fields 
 * TODO: complete this hook by wiring up the form store
 * @returns 
 */
function useFields() {

    return { fields: INIT_FIELDS, }
}

export default useFields;