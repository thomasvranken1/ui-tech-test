import { useReducer } from "react";
import { useSearchParams } from "react-router-dom";

type Mode = "preview" | "edit";
const DEFAULT_MODE: Mode = "preview";

function isMode(input: string | null): input is Mode {
    return input === "preview" || input === "edit";
}

export default function useMode() {
    const [searchParams, setSearchParams] = useSearchParams();
    return useReducer((mode: Mode) => {
        let newMode = searchParams.get("mode");
        if (!isMode(newMode)) {
            newMode = DEFAULT_MODE;
        }
        newMode = (mode === "edit" ? "preview" : "edit")
        /*
        FIXME The combination of setSearchParams inside useReducer triggers an error in React:
        Cannot update a component (`RouterProvider`) while rendering a different component (`ModeSwitch`)
        I did not find a solution to this problem. Relevant articles:
        - https://github.com/facebook/react/issues/18178
        - https://gorannikolovski.com/blog/cannot-update-a-component-while-rendering-a-different-component-react-native
        - https://stackoverflow.com/questions/62336340/cannot-update-a-component-while-rendering-a-different-component-warning
        */
        setSearchParams({ mode: (newMode) });
        return newMode as Mode;
    }, DEFAULT_MODE);
}
